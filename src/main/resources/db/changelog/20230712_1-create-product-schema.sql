--liquibase formatted sql
--changeset microservice_class:20230721

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE PRODUCT
(
    PRODUCT_ID          uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT_CODE        varchar(255) NOT NULL UNIQUE ,
    PRODUCT_NAME        varchar(255) NOT NULL,
    PRODUCT_DESCRIPTION varchar(255),
    DAILY_LIMIT         numeric          DEFAULT 0,
    MONTHLY_LIMIT       numeric          DEFAULT 0,
    CREATED_AT          timestamp,
    UPDATED_AT          timestamp
);

CREATE TABLE PRODUCT_EVENT
(
    EVENT_ID                uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT_ID              uuid,
    PRODUCT_CODE            varchar(255) NOT NULL,
    PRODUCT_LIMIT_TYPE      varchar(255) NOT NULL, -- DAILY, MONTHLY
    PRODUCT_LIMIT_AMOUNT    numeric          DEFAULT 0,
    PRODUCT_LAST_UPDATED_AT timestamp,
    EVENT_CREATED_AT        timestamp
);